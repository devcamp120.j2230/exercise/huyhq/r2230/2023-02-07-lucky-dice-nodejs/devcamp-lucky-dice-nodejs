const { default: mongoose } = require("mongoose");

const voucherModel = require("../model/Voucher");

const createVoucher = (req, res) => {
    let body = req.body;
    if (!body.code) {
        return res.status(400).json({
            message: `Error 400: Code phai bat buoc!`
        })
    }

    if (!body.discount) {
        return res.status(400).json({
            message: `Error 400: Discount phai bat buoc!`
        })
    }

    if (!Number.isInteger(body.discount) || body.discount<0 || body.discount>100) {
        return res.status(400).json({
            message: `Error 400: Discount khong dung!`
        })
    }

    const newVoucher = new voucherModel({
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note,
    })

    voucherModel.create(newVoucher, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(201).json({
                message: `Create Voucher successfull!`,
                Voucher: data
            })
        }
    })
};

const getAllVoucher = (req, res) => {
    voucherModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Load data successfull!`,
                Voucher: data
            })
        }
    })
};

const getVoucherById = (req, res) => {
    let voucherId = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: `Error 400: Id Voucher khong dung!`
        })
    }

    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Load data successfull!`,
                Voucher: data
            })
        }
    })
};
const updateVoucherById = (req, res) => {
    let voucherId = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: `Error 400: Id Voucher khong dung!`
        })
    }
    let body = req.body;
    if (!body.code) {
        return res.status(400).json({
            message: `Error 400: Code phai bat buoc!`
        })
    }

    if (!body.discount) {
        return res.status(400).json({
            message: `Error 400: Discount phai bat buoc!`
        })
    }

    if (!Number.isInteger(body.discount) || body.discount<0 || body.discount>100) {
        return res.status(400).json({
            message: `Error 400: Discount khong dung!`
        })
    }

    const voucher = new voucherModel({
        code: body.code,
        discount: body.discount,
        note: body.note,
    })

    voucherModel.findByIdAndUpdate(voucherId, voucher, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(200).json({
                message: `Update Voucher successfull!`,
                Voucher: data
            })
        }
    })
};
const deleteVoucherById = (req, res) => {
    let voucherId = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            message: `Error 400: Id Voucher khong dung!`
        })
    }

    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: `Error 500: ${error.message}`
            })
        } else {
            return res.status(204).json({
                message: `Delete Voucher successfull!`,
                Voucher: data
            })
        }
    })
};

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}