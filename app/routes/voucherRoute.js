const express = require("express");

const {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
} = require("../controller/VoucherController");

const voucherRouter = express.Router();

voucherRouter.get("/voucher", getAllVoucher);
voucherRouter.post("/voucher", createVoucher);
voucherRouter.get("/voucher/:voucherId", getVoucherById);
voucherRouter.put("/voucher/:voucherId", updateVoucherById);
voucherRouter.delete("/voucher/:voucherId", deleteVoucherById);

module.exports = {voucherRouter};