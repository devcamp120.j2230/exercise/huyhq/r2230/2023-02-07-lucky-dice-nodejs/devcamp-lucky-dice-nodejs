const express = require("express");

const {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
} = require("../controller/diceHistoryController");

const diceHistoryRouter = express.Router();

diceHistoryRouter.get("/dice", getAllDiceHistory);
diceHistoryRouter.post("/dice", createDiceHistory);
diceHistoryRouter.get("/dice/:diceId", getDiceHistoryById);
diceHistoryRouter.put("/dice/:diceId", updateDiceHistoryById);
diceHistoryRouter.delete("/dice/:diceId", deleteDiceHistoryById);

module.exports = {diceHistoryRouter};